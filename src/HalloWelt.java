import java.util.InputMismatchException;
import java.util.Scanner;

public class HalloWelt
{
    String name = "";
    Geschlecht geschlecht;
    int alter = 0;
    double wiederholungen = 3;

    public static void main(String[] args)
    {
        HalloWelt halloWelt = new HalloWelt();
        try
        {
            if (halloWelt.eingabe())
            {
                halloWelt.ausgabe();
            }
        } catch (InputMismatchException execption)
        {
            System.out.println("Die Eingabe konnte nicht ausgewertet werden");
        }
    }

    /**
     * Eingabe Methode für den Namen, das Geschlecht und das Alter
     *
     * @return Boolean der Auskunft gibt ob die Funktion erfolgreich ausgeführt wurde
     */
    private boolean eingabe() throws InputMismatchException
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Geben Sie Ihren Namen an: ");
        name = sc.next();
        if (!name.matches("^[a-zA-Z]+$")) throw new InputMismatchException();

        System.out.println("Sind Sie (M)ännlich oder (W)eiblich?");
        String choice = sc.next();

        if (choice.equalsIgnoreCase("M"))
        {
            geschlecht = Geschlecht.maennlich;
        } else if (choice.equalsIgnoreCase("W"))
        {
            geschlecht = Geschlecht.weiblich;
        } else
        {
            throw new InputMismatchException();
        }

        System.out.println("Geben sie ihr Alter an: ");
        alter = sc.nextInt();
        sc.close();

        return true;
    }


    /**
     * Ausgabe Funktion
     */
    private void ausgabe()
    {
        if ((alter / 4.0) > 3.0)
        {
            wiederholungen = Math.round(alter / 4.0);
        }

        for (int i = 1; i <= wiederholungen; i++)
        {
            if (geschlecht == Geschlecht.maennlich&& alter >= 18)
            {
                System.out.println("Guten Tag Herr " + name + "!");
            } else if (geschlecht == Geschlecht.weiblich&& alter >= 18)
            {
                System.out.println("Guten Tag Frau " + name + "!");
            } else
            {
                System.out.println("Hallo " + name + "!");
            }
        }
    }

    private enum Geschlecht
    {
        maennlich, weiblich
    }
}
