public class Rechner
{
    public static void main(String[] args)
    {
        Rechner rechner = new Rechner();

        rechner.aufgabeA();
        System.out.println();
        rechner.aufgabeB();
    }

    public void aufgabeA()
    {
        System.out.println("Aufgabe  A:");

        int n = 1;
        while (n <= 10)
        {
            System.out.println("" + n + " x " + n + " x " + n + " = " + (n * n * n));
            n++;
        }
    }

    public void aufgabeB()
    {
        System.out.println("Aufgabe B:");

        int n = 1;
        while (n < 8)
        {
            System.out.println("" + (n) + " x " + (n + 1) + " x " + (n + 2) + " = " + (n * (n + 1) * (n + 2)));
            n++;
        }
    }
}
