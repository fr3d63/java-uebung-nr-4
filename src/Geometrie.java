import java.util.InputMismatchException;
import java.util.Scanner;

public class Geometrie
{
    static Scanner eingabe = new Scanner(System.in);

    /**
     * Ruft die userEingabe Methode auf
     * und behandelt evtl Fehler durch falsche User Eingaben
     */
    public static void main(String[] args)
    {
        Geometrie flaechenRechner = new Geometrie();
        boolean running = true;
        String choice;

        try
        {
            do
            {
                System.out.println("Ergebnis: " + flaechenRechner.userEingabe());
                System.out.println("Möchten Sie eine weitere Berechnung ausführen? (y/n)");
                choice = eingabe.next();
                if (choice.equalsIgnoreCase("n"))
                    running = false;
            } while (running);
        } catch (InputMismatchException ex)
        {
            System.out.println("Fehler: Ihre Eingabe konnte nicht ausgewertet werden!");
        }
    }

    /**
     * Fragt den User was er berechnen möchte und erfasst alle notwendigen Daten
     *
     * @return Ergebnis der Berechnungen
     * @throws InputMismatchException
     */
    private double userEingabe() throws InputMismatchException
    {
        int auswahl;
        System.out.println("Was möchten Sie berechnen? 1 Kreis 2 quadrat 3 dreieck");
        auswahl = eingabe.nextInt();

        switch (auswahl)
        {
            case 1:
                System.out.println("Bitte geben Sie den Radius an:");
                return kreis(eingabe.nextDouble());
            case 2:
                System.out.println("Geben Sie die Seitenlänge an:");
                return quadrat(eingabe.nextDouble());
            case 3:
                System.out.println("Geben Sie die Grundfläche an:");
                double gFlaeche = eingabe.nextDouble();

                System.out.println("Geben Sie die Höhe an:");
                double hoehe = eingabe.nextDouble();

                return dreieck(gFlaeche, hoehe);
            default:
                throw new InputMismatchException();
        }
    }

    /**
     * Berechnet eine Kreisfläche
     *
     * @param radius Radius des Kreises
     * @return Fläche des Kreises
     */
    private double kreis(double radius)
    {
        return (Math.PI * Math.pow(radius, 2));
    }

    /**
     * Berechnet eine Quadratfläche
     *
     * @param seitenlaenge Länge einer Seite des Quadrats
     * @return Fläche des Quadrats
     */
    private double quadrat(double seitenlaenge)
    {
        return (seitenlaenge * seitenlaenge);
    }

    /**
     * Berechnet die Fläche eines Dreiecks
     *
     * @param grundflaeche Grundfläche des Dreiecks
     * @param hoehe        Höhe auf der Grundfläche
     * @return Fläche des Dreiecks
     */
    private double dreieck(double grundflaeche, double hoehe)
    {
        return ((grundflaeche * hoehe) / 2);
    }

}
