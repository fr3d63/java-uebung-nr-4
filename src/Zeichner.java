public class Zeichner
{
    /**
     * Ruft die einzelnen Funktionen für diie Aufgaben auf
     */
    public static void main(String[] args)
    {
        Zeichner formen = new Zeichner();

        System.out.println("Aufgabe A");
        formen.aufgabeA(8, 16);

        System.out.println("Aufgabe B");
        formen.aufgabeB(8, 16);

        System.out.println("Aufgabe C");
        formen.aufgabeC(8, 16);

        System.out.println("Aufgabe D");
        formen.aufgabeD(8, 16);

        System.out.println("Aufgabe E");
        formen.aufgabeE(8, 16);

        System.out.println("Aufgabe F");
        formen.aufgabeF(8, 16);
    }

    /**
     * Zeichnet das in Aufgabe A festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeA(int zeilen, int spalten)
    {
        for (int i = 1; i <= zeilen; i++)
        {
            for (int x = 1; x <= spalten; x++)
            {
                System.out.print("+");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Zeichnet das in Aufgabe B festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeB(int zeilen, int spalten)
    {
        boolean isMinus = false;

        for (int i = 1; i <= zeilen; i++)
        {
            if (!isMinus)
            {
                for (int x = 1; x <= spalten; x++)
                {
                    System.out.print("+");
                    isMinus = true;
                }
            } else
            {
                for (int x = 1; x <= spalten; x++)
                {
                    System.out.print("-");
                    isMinus = false;
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * Zeichnet das in Aufgabe C festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeC(int zeilen, int spalten)
    {
        boolean isMirrored = false;
        boolean isPoint = true;

        for (int z = 0; z < zeilen; z++)
        {
            if (z < zeilen / 2)
            {
                for (int i = 0; i < spalten / 2; i++)
                {
                    System.out.print(".");
                }
                for (int i = 0; i < spalten / 2; i++)
                {
                    System.out.print("+");
                }
            } else
            {
                for (int i = 0; i < spalten / 2; i++)
                {
                    System.out.print("+");
                }
                for (int i = 0; i < spalten / 2; i++)
                {
                    System.out.print(".");
                }
            }
            System.out.println();
        }
    }

    /**
     * Zeichnet das in Aufgabe D festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeD(int zeilen, int spalten)
    {
        int countPlus = 0;

        for (int i = 0; i <= zeilen; i++)
        {
            for (int j = 0; j < (spalten - countPlus); j++)
            {
                System.out.print(".");
            }
            for (int j = 0; j < countPlus; j++)
            {
                System.out.print("+");
            }
            countPlus = countPlus + 2;
            System.out.println();
        }
    }

    /**
     * Zeichnet das in Aufgabe E festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeE(int zeilen, int spalten)
    {
        int zeile;
        int spalte;

        for (zeile = 1; zeile <= zeilen; zeile++)
        {
            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
            {
                System.out.print(".");
            }

            for (spalte = 1; spalte <= (2 * zeile - 1); spalte++)
            {
                System.out.print("+");
            }

            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
            {
                System.out.print(".");
            }
            System.out.println();
        }

    }

    /**
     * Zeichnet das in Aufgabe F festgelegte Muster in Abhängigkeit der Parameter
     *
     * @param zeilen  Anzahl der Zeilen
     * @param spalten Anzahl der Spalten
     */
    private void aufgabeF(int zeilen, int spalten)
    {
        int zeile;
        int spalte;
        boolean isO = false;

        for (zeile = 1; zeile <= zeilen; zeile++)
        {
            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
            {
                System.out.print(".");
            }

            for (spalte = 1; spalte <= (2 * zeile - 1); spalte++)
            {
                if (zeile > 1 && !isO)
                {
                    System.out.print("+");
                    isO = true;
                } else if (zeile > 1 && isO)
                {
                    System.out.print("o");
                    isO = false;
                } else
                {
                    System.out.print("+");
                    isO = false;
                }
            }
            isO = false;

            for (spalte = 1; spalte <= zeilen - zeile; spalte++)
            {
                System.out.print(".");
            }
            System.out.println();
        }
    }
}
