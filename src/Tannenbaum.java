import java.util.Scanner;

/**
 * Klasse Tannenbaum
 */
public class Tannenbaum
{
    static Scanner eingabeScanner = new Scanner(System.in);

    /**
     * Setzt die Höhe des Baumes und die Länge des Stammes
     */

    public static void main(String[] args)
    {
        Tannenbaum weihnachtsbaum = new Tannenbaum();
        int baumHoehe;
        int stammLaenge;
        System.out.println("Geben Sie die Höhe des Baumes an:");

        baumHoehe = eingabeScanner.nextInt();
        System.out.println("Gewählte Höhe: " + baumHoehe);
        eingabeScanner.close();
        weihnachtsbaum.baumZeichnen(baumHoehe, 2);
    }

    /**
     * Zeichnet den Baum abhängig von der Höhe des Baumes und der Länge des Stammes
     *
     * @param baumhoehe   Hoehe des Baumes
     * @param stammLaenge Laenge des Stammes
     */
    public void baumZeichnen(int baumhoehe, int stammLaenge)
    {
        int zeile;
        int spalte;

        for (zeile = 1; zeile <= baumhoehe; zeile++)
        {
            for (spalte = 1; spalte <= baumhoehe - zeile; spalte++)
            {
                System.out.print(" ");
            }

            for (spalte = 1; spalte <= (2 * zeile - 1); spalte++)
            {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int i = 0; i < stammLaenge; i++)
        {
            for (zeile = 1; zeile < baumhoehe; zeile++)
            {
                System.out.print(" ");
            }
            System.out.println("*");
        }
    }
}
