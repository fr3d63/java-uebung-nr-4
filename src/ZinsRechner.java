import java.util.Scanner;

public class ZinsRechner
{

    /**
     * Fragt den User nach relavanten Daten und berechnet die Aufgaben
     */
    public static void main(String[] args)
    {
        ZinsRechner zinsRechner = new ZinsRechner();
        Scanner eingabe = new Scanner(System.in);
        double startKapital;
        double zinssatz;
        int jahre = 10;

        System.out.println("Geben Sie das Startkapital an: ");
        startKapital = eingabe.nextDouble();

        System.out.println("Geben Sie den Zinssatz an: ");
        zinssatz = eingabe.nextDouble();

        eingabe.close();

        System.out.println("Das Kapital nach 10 Jahren ist: " + zinsRechner.aufgabeA(startKapital, zinssatz, jahre) + " Euro");
        System.out.println("Nach " + zinsRechner.aufgabeB(startKapital, zinssatz) + " Jahren hat es sich verdoppelt");
    }

    /**
     * Berechnet ein Endkapital mit Zinseszins
     *
     * @param startKapital Das Startkapital
     * @param prozentSatz  Der Prozentsatz zu dem verzinst wird
     * @param jahre        Anzahl der Jahre
     * @return Endkapital
     */
    private double aufgabeA(double startKapital, double prozentSatz, double jahre)
    {
        return (Math.round(startKapital * Math.pow((1 + (prozentSatz / 100)), jahre)));
    }

    /**
     * Berechnet nach wie vielen Jahren sich ein Kapital verdoppelt hat
     *
     * @param startKapital Das Startkapital
     * @param prozentSatz  Der Prozentsatz zu dem verzinst wird
     * @return Anzahl der Jahre nach denen sich das Kapital verdoppelt hat
     */
    private double aufgabeB(double startKapital, double prozentSatz)
    {
        double jahre = 1.0;
        double endKapital;

        do
        {
            endKapital = Math.round(startKapital * Math.pow((1 + (prozentSatz / 100)), jahre));
            jahre = jahre + 1;
        } while (endKapital <= startKapital * 2);
        return jahre;
    }

}
