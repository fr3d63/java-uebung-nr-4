import java.util.Scanner;

public class Zahlenraten
{
    static Scanner sc = new Scanner(System.in);
    final double ZUFALLSZAHL;

    /**
     * Konstruktor
     *
     * @param zufallszahl Gewünschte Zufallszahl
     */
    public Zahlenraten(double zufallszahl)
    {
        this.ZUFALLSZAHL = zufallszahl;
    }

    public static void main(String[] args)
    {
        Zahlenraten zahlenraten = new Zahlenraten(Math.round(Math.random() * 100));
        int versuche = 1;
        boolean erraten = false;
        while (!erraten && (versuche < 9))
        {
            erraten = zahlenraten.raten();
            versuche++;
        }
        zahlenraten.ausgabe(versuche);
    }

    /**
     * Methode die Eingaben annimt und diese mit der ZUFALLSZAHL vergleich
     *
     * @return Boolean, ob die Zahl richtig war oder nicht
     */
    private boolean raten()
    {
        System.out.println("Geben sie eine Zahl ein: ");
        double benutzerEingabe = sc.nextDouble();
        return benutzerEingabe == ZUFALLSZAHL;
    }

    /**
     * Ausgabe auf den Bildschirm abh�ngig von der Anzahl der Verusche
     *
     * @param versuche Integer mit der Anzahl der Versuche
     */
    private void ausgabe(int versuche)
    {
        if (versuche <= 3)
        {
            System.out.println("Einfach toll gemacht. Nahezu genial!");
        } else if (versuche <= 6)
        {
            System.out.println("Gut gemacht! Sie sind fast wie Einstein.");
        } else if (versuche <= 8)
        {
            System.out.println("War wohl doch nicht so leicht...");
        } else
        {
            System.out.println("Üben üben üben! Es wird schon!");
        }
    }
}

