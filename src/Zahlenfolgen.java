public class Zahlenfolgen
{

    public static void main(String[] args)
    {
        Zahlenfolgen zahlenfolgen = new Zahlenfolgen();

        System.out.println("Folge A");
        zahlenfolgen.folgeA(50);

        System.out.println("Folge B");
        zahlenfolgen.folgeB(99);

        System.out.println("Folge C");
        zahlenfolgen.folgeC(20);

        System.out.println("Folge D");
        zahlenfolgen.folgeD(50);
    }


    private void folgeA(final int LIMIT)
    {
        for (int i = 1; i <= LIMIT; i++)
        {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private void folgeB(final int LIMIT)
    {
        for (int i = 1; i <= LIMIT; i = i + 2)
        {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private void folgeC(final int LIMIT)
    {
        int[] zahlen = new int[LIMIT];
        zahlen[0] = 0;
        zahlen[1] = 1;

        for (int i = 2; i < LIMIT; i++)
        {
            zahlen[i] = zahlen[i - 1] + zahlen[i - 2];
            System.out.print(zahlen[i] + " ");
        }
        System.out.println();
    }

    private void folgeD(final int LIMIT)
    {
        final int EINS = 1;
        final int DREI = 3;
        int i = 1;

        System.out.print(i + " ");
        while (i <= LIMIT)
        {
            if (i % 2 == 0)
            {
                System.out.print((i + EINS) + " ");
                i = i + EINS;
            } else
            {
                System.out.print((i + DREI) + " ");
                i = i + DREI;
            }
        }
    }
}
